FROM alpine:latest

ARG VERSION=0.1.0

RUN apk update && apk add wget git bash
RUN wget https://releases.hashicorp.com/nomad-pack/0.1.0/nomad-pack_${VERSION}_linux_amd64.zip -P /tmp
RUN unzip -d /usr/bin /tmp/nomad-pack_${VERSION}_linux_amd64.zip 
RUN rm /tmp/nomad-pack_${VERSION}_linux_amd64.zip 

RUN addgroup -S nomad && adduser -S np -G nomad -h /home/np
USER np

WORKDIR /home/np